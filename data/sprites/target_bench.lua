local tbench = {}

local s = sol.surface.create('menus/solarus_logo.png')
local targets = {}

local target_count = 10000
for i = 1,target_count do
  table.insert(targets, sol.surface.create(256,256))
end

local rects = {
  {0,45,46,46},
  {46,45,46,46},
  {92,49,39,39}
}

local sprite_count = 100000
local num_frames = 0
function tbench:on_started()
  local start_time = os.clock()
  sol.timer.start(self,3000,function()
      sol.menu.stop(tbench)
      local time_delta = os.clock() - start_time
      print(num_sprite,"random surface FPS :", num_frames/time_delta)
    end)
end

local rl = #rects

function tbench:on_draw(dst)
  dst:fill_color{128,128,128}
  num_frames = num_frames + 1
  for i = 1,target_count-1 do
    local target = targets[i]
    for j = 1,sprite_count/target_count do
      local rect = rects[math.random(rl)]
      local w,h = dst:get_size()
      local x,y = math.random(w),math.random(h)
      s:draw_region(rect[1],rect[2],rect[3],rect[4],target,x,y)
    end
    target:draw(dst)
  end
end

return tbench