local sbench = {}

local clock = require'socket'.gettime
local s = sol.surface.create('menus/solarus_logo.png')
local shader1 = sol.shader.create('default')
s:set_shader(shader1)

local rects = {
  {0,45,46,46},
  {46,45,46,46},
  {92,49,39,39}
}

local logfile = assert(io.open('shader_bench.dat','w'))
logfile:write('# sprite_count, fps\n')

local callbacks = {}

local function add_rt_timer(delay,closure)
  callbacks[closure] = clock() + delay
end

local function update_rt_timer()
  local current_time = clock()
  for callback,target_time in pairs(callbacks) do
    if current_time >= target_time then
      callback()
      callbacks[callback] = nil
    end
  end
end

local max_sprites = 100000
local min_sprites = 10000
local inc = 10000
local power = 1.5
local sample_duration_s = 2
local num_sprite = min_sprites
local num_frames = 0
function sbench:on_started()
  local start_time = clock()
  local function bench_with(num)
    return function()
      local current_time = clock()
      local time_delta = current_time - start_time
      print(num_sprite,"random surface w/shader FPS :", num_frames/time_delta)
      logfile:write(num_sprite,' ',num_frames/time_delta,'\n')
      if num_sprite >= max_sprites then
        sol.menu.stop(sbench)
        logfile:close()
        return 
      end
      start_time = current_time
      num_sprite = num
      num_frames = 0
      
      add_rt_timer(sample_duration_s,bench_with(num+inc))
    end
  end
  add_rt_timer(sample_duration_s,bench_with(num_sprite))
end

local rl = #rects

function sbench:on_draw(dst)
  update_rt_timer()
  dst:fill_color{128,128,128,20}
  num_frames = num_frames + 1
  for i = 1,num_sprite do
    local rect = rects[math.random(rl)]
    local w,h = dst:get_size()
    local x,y = math.random(w),math.random(h)
    s:draw_region(rect[1],rect[2],rect[3],rect[4],dst,x,y)
  end
end

return sbench