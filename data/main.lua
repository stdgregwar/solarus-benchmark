-- This is the main Lua script of your project.
-- You will probably make a title screen and then start a game.
-- See the Lua API! http://www.solarus-games.org/doc/latest

local solarus_logo = require("scripts/menus/solarus_logo")
local surface_bench = require("scripts/menus/surface_bench")
local target_bench = require("scripts/menus/target_bench")
local shader_bench = require("scripts/menus/shader_bench")

function menu_chain(menu,n,...)
  local nexts = {...}
  if n then
    function menu:on_finished()
      menu_chain(n,unpack(nexts))
    end
  else
    function menu:on_finished()
      sol.main.exit()
    end
  end
  sol.menu.start(sol.main,menu)
end

-- This function is called when Solarus starts.
function sol.main:on_started()

  -- Setting a language is useful to display text and dialogs.
  sol.language.set_language("en")
  
  menu_chain(solarus_logo,shader_bench,surface_bench,target_bench)
end

-- Event called when the player pressed a keyboard key.
function sol.main:on_key_pressed(key, modifiers)

  local handled = false
  if key == "f5" then
    -- F5: change the video mode.
    sol.video.switch_mode()
    handled = true
  elseif key == "f11" or
    (key == "return" and (modifiers.alt or modifiers.control)) then
    -- F11 or Ctrl + return or Alt + Return: switch fullscreen.
    sol.video.set_fullscreen(not sol.video.is_fullscreen())
    handled = true
  elseif key == "f4" and modifiers.alt then
    -- Alt + F4: stop the program.
    sol.main.exit()
    handled = true
  elseif key == "escape" and sol.main.game == nil then
    -- Escape in title screens: stop the program.
    sol.main.exit()
    handled = true
  end

  return handled
end

-- Starts a game.
function sol.main:start_savegame(game)

  -- Skip initial menus if any.
  sol.menu.stop(solarus_logo)

  sol.main.game = game
  game:start()
end
